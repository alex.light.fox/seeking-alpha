import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import { presets, getNewAlive } from './lib';
import { Set } from 'immutable';

describe('<App/>', () => {
  it('renders without crashing', () => {
    const div = document.createElement('div');
    ReactDOM.render(<App />, div);
  });
})

describe('TICKS', () => {
  Object.keys(presets).filter(key => key !== 'randomFill').forEach(key => {
    it('Ticks for ' + key, () => {
      expect(getNewAlive(presets[key])).toMatchSnapshot();
    });
  })
})