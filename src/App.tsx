import React from 'react';
import { gridSize }from './config';
import I, { Set } from 'immutable';
import { getNewAlive, presets } from './lib'

function App() {
  const [ alive, setState ] = React.useState<Set<number>>(I.Set([]));

  function tick(){
    setState(getNewAlive(alive));
  }

  function loadPreset(values: Set<number>){
    setState(values);
  }

  const setIndex = (k: number) => {
    setState((prevState) => ({
      ...prevState,
      alive: alive.has(k) ? alive.delete(k) : alive.add(k)
    }));
  }

  const grid: JSX.Element[] = [];

  for(let i = 0; i < gridSize * gridSize; i++){
    grid.push(
        <div
            title={`${i}`}
            onClick={() => setIndex(i)}
            key={i} className={alive.has(i) ? 'aliveCell' : 'deadCell' }
        />
    );
  }

  console.log(alive.toArray());

  return (
    <div>
      <div>
        <div>
          <button type="button" onClick={tick} children="TICK"/>
        </div>
        <div>
          { Object.keys(presets).map((key: keyof typeof presets) => (<button
            key={key}
            type="button"
            onClick={() => loadPreset(presets[key])}
            children={key}
            />)) }
        </div>
      </div>
      <div className="grid">
        { grid }
      </div>
    </div>
  );
}

export default App;
