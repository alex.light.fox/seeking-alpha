import { gridSize } from './config'
import I, { Set } from 'immutable';

const neighborIndexes: number[][] = Array(gridSize*gridSize).fill([]);

for (let x = 0; x < gridSize; x++){
  for(let y = 0; y < gridSize; y++) {
    let index = y * gridSize + x;
    neighborIndexes[index] = [
      [x - 1, y - 1],
      [x, y - 1],
      [x + 1, y - 1],
      [x - 1, y],
      [x + 1, y],
      [x - 1, y + 1],
      [x, y + 1],
      [x + 1, y + 1],
    ].filter(cords => Math.min(...cords) >= 0 && Math.max(...cords) < gridSize )
      .map(([ px, py ]) => py * gridSize + px)
  }
}

export function getNewAlive(alive: Set<number>){

  let activeCells = alive
      .concat(alive
          .toArray()
          .reduce((acc: number[], v: number ) => acc.concat(...neighborIndexes[v]), [])
      );

  // let result = activeCells.filter(i => {
  //   const neighbors = neighborIndexes[i]
  //       .reduce((p: number, n: number) => alive.has(n) ? p + 1 : p, 0);
  //   if(neighbors === 3) return true;
  //   if(neighbors === 2) return alive.has(i);
  //   return false;
  // });

  // ~ x2 faster
  const tempGrid = Array(gridSize * gridSize).fill(false);
  alive.forEach(i => tempGrid[i] = true);

  let result = activeCells.filter(i => {
    const neighbors = neighborIndexes[i]
        .reduce((p: number, n: number) => tempGrid[n] ? p + 1 : p, 0);
    if(neighbors === 3) return true;
    if(neighbors === 2) return tempGrid[i];
    return false;
  }).sort();


  return result;
}

const randomFill = I.Set(Array.from(Array(2500).keys()).filter(() => Math.random() > 0.5));
const line = I.Set([51,52,53]);
const square = I.Set([51,52,101,102]);
const worm = I.Set([351, 352, 353, 303, 252]);
const twins = I.Set([1202, 1201, 1203, 1152, 1153, 1154]);
const image = I.Set([
  192, 480, 292, 580, 485, 230, 487, 585, 682, 235, 587,
  683, 332, 492, 684, 237, 333, 334, 432, 592, 688, 433, 689,
  82, 242, 338, 434, 530, 690, 83, 339, 84, 180, 340, 438, 439,
  535, 88, 280, 440, 89, 185, 537, 90, 187, 285, 542, 287
]);

export const presets: Record<string, Set<number>> = {
  randomFill,
  line,
  square,
  worm,
  twins,
  image
};